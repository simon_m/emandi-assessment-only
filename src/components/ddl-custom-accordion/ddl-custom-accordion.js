define(function(require) {

    var AccordionView = require('components/adapt-contrib-accordion/js/adapt-contrib-accordion');
    var Adapt = require('coreJS/adapt');

    var DDLAccordion = AccordionView.extend({

        ///DDL addition Septempber 2016
        ///Override open item function from original component
        ///To fix resize bug on initial accordion item open
        openItem: function($itemEl) {
            if (!$itemEl) {
                return false;
            }

            var $body = $('.accordion-item-body', $itemEl).first();
            var $button = $('button', $itemEl).first();
            var $icon = $('.accordion-item-title-icon', $itemEl).first();

            $body = $body.stop(true, true).slideDown(this.toggleSpeed, function() {
                $(window).resize();
                $body.a11y_focus();
            });

            $button.addClass('selected');
            $button.attr('aria-expanded', true);

            this.setVisited($itemEl.index());
            $button.addClass('visited');

            $icon.removeClass('icon-plus');
            $icon.addClass('icon-minus');
        }
    });

    Adapt.register('ddl-accordion', DDLAccordion);

    return DDLAccordion;

});
